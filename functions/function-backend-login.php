<?php
/*Change the wp-admin logo*/
function change_my_wp_login_image() {
	echo "
	<style>
	body.login #login h1 a {
	background: url('".get_bloginfo('template_url')."/assets/images/logo.png') center no-repeat rgba(250,250,250,.10);
	height:65px;
	width:100%; 
	margin-bottom: 10px;
	background-size: contain; }
	</style>
	";
}
add_action("login_head", "change_my_wp_login_image");

/*Change the wp-admin logo link*/
function loginpage_custom_link() {
	return site_url();
}
add_filter('login_headerurl','loginpage_custom_link');

/*Change the wp-admin logo title*/
function change_title_on_logo() {
	return site_url();
}
add_filter('login_headertitle', 'change_title_on_logo');