<?php
/**
 * Custom Post Types
 */
if ( ! function_exists( 'customPostTypes' ) ) {

	function customPostTypes() {

		$labels = array(
			'name' 					=> 'Post Types',
			'singular_name'			=> 'Post Type',
			'menu_name' 			=> 'Post Types',
			'add_new' 				=> 'Add New',
			'add_new_item' 			=> 'Add New Post Type',
			'new_item' 				=> 'New Post Type',
			'edit_item' 			=> 'Edit Post Type',
			'view_item' 			=> 'View Post Type',
			'all_items' 			=> 'All Post Types',
			'search_items' 			=> 'Search Post Types',
			'parent_item_colon' 	=> 'Parent Post Types:',
			'not_found' 			=> 'Nothing found.',
			'not_found_in_trash'	=> 'Nothing found in Trash.',
		);
		
		register_post_type(
			'posttype', 
			array(
				'labels' 				=> $labels,
				'public' 				=> true,
				'publicly_queryable' 	=> true,
				'show_ui' 				=> true,
				'query_var' 			=> true,
				'show_in_menu'			=> true,
				'show_in_nav_menus'		=> true,
				'show_in_admin_bar'   	=> false,
				'can_export'			=> true,
				'exclude_from_search' 	=> false,
				'has_archive' 			=> true,
				'hierarchical'			=> false,
				'capability_type' 		=> 'post',
				'menu_icon'   			=> 'dashicons-download',
				'supports' 				=> array('title', 'editor', 'excerpt', 'thumbnail'),
			)
		);
	}
	
	add_action('init', 'customPostTypes');
}