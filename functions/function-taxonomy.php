<?php
/**
 * Custom Taxonomies
 */
if ( ! function_exists( 'registerTaxonomies' ) ) {

	function registerTaxonomies() {	
		
		$taxonomyArray = array(
			'name' 					=> _x('Taxonomies', 'taxonomy general name'),
			'singular_name' 		=> _x('Taxonomy', 'taxonomy singular name'),
			'add_new_item' 			=> __('Add New Taxonomy'),
			'new_item_name' 		=> __('New Taxonomy'),
			'edit_item' 			=> __('Edit Taxonomy'),
			'update_item' 			=> __('Update Taxonomy'),
			'all_items' 			=> __('All Taxonomies'),
			'search_items' 			=> __('Search Taxonomies'),
			'parent_item' 			=> __('Parent Taxonomy'),
			'parent_item_colon' 	=> __('Parent Taxonomies:'),
			'menu_name' 			=> __('Taxonomies'),
		);
		register_taxonomy(
			'taxonomy-cat',
			array(
				'posttype'
			),
			array(
				'hierarchical' 		=> true,
				'labels' 			=> $taxonomyArray,
				'show_ui' 			=> true,
				'show_admin_column' => true,
				'query_var' 		=> true,
				'rewrite'           => array('slug' => 'taxonomy-slug'),
			)
		);
	}

	add_action('init', 'registerTaxonomies');
}