<?php
/**
 * Include default scripts and css
 * Include in wp_header()
 **/
if ( ! function_exists( 'enqueStylesAndScripts' ) ) {

	function enqueStylesAndScripts() {
		/* 
		 * Register/Hook Styles
		 */	
		wp_enqueue_style('bootstrap_css',get_stylesheet_directory_uri().'/vendor/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('stylesheet',get_stylesheet_uri());
		wp_enqueue_style('responsive',get_stylesheet_directory_uri().'/assets/css/responsive.css');
		wp_enqueue_style('normalize',get_stylesheet_directory_uri().'/assets/css/normalize.css');
		
		/* 
		 * Register/Hook Scripts
		 */
		wp_enqueue_script( 'jquery_js', get_template_directory_uri().'/assets/js/jquery-1.11.0.min.js',array(),'',true);
		wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'/vendor/bootstrap/js/bootstrap.min.js',array(),'',true);
		wp_enqueue_script( 'smoothscroll', get_template_directory_uri().'/vendor/smoothscroll/smoothscroll.js',array(),'',true);
		wp_enqueue_script( 'template_scripts', get_template_directory_uri().'/assets/js/script-theme.js',array(),'',true);
		wp_enqueue_script( 'ajax_scripts', get_template_directory_uri().'/assets/js/script-ajax.js',array(),'',true);
	}
	
	add_action('wp_enqueue_scripts','enqueStylesAndScripts');
}

if ( ! function_exists( 'enqueStylesAndScripts' ) ) {

	function enqueStylesAndScripts() {

		/* 
		 * Register/Hook Scripts
		 */
		wp_enqueue_script( 'custom_admin_script', get_template_directory_uri().'/assets/js/script-admin.js', array( 'jquery' ) );
	}

	add_action( 'admin_enqueue_scripts', 'enqueStylesAndScripts' );
}