<?php
if ( ! function_exists( 'addCategoryInPage' ) ) {

	function addCategoryInPage() {  

		register_taxonomy_for_object_type( 'category', 'page' );
	}

	add_action( 'init', 'addCategoryInPage' );
}