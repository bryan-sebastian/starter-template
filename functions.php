<?php
/**
 * Project Name Theme
 *
 * This custom wordpress theme was designed and developed by iConcept Global Advertising Inc.
 * Every element found in the layout of the theme and the functionality was checked and approved by the website owner.
 * It was based on what kind of website the owner wants and what functionality of his/her website is.
 *
 * Copyright @ Year
 **/


/* custom functions */
include_once( 'functions/function-view.php' );
include_once( 'functions/function-after-setup-theme.php' ); // Inside of this file change the template_name with your template name
include_once( 'functions/function-menus.php' ); // Inside of this file change the template_name with your template name
include_once( 'functions/function-theme-support.php' );
// include_once( 'functions/function-widgets.php' ); // Inside of this file change the template_name with your template name
include_once( 'functions/function-enqueue.php' );
include_once( 'functions/function-body-class.php' );
include_once( 'functions/function-wp-title.php' );
include_once( 'functions/function-backend-login.php' ); // Set the image in your back-end log-in page
// include_once( 'functions/function-category-page.php' ); // Uncomment this line to add general category in your page 
// include_once( 'functions/function-post-type.php' );
// include_once( 'functions/function-taxonomy.php' );
// include_once( 'functions/function-option.php' ); // Adds ACF option page/subpage in the backend.
// include_once( 'functions/function-term-settings.php' ); // Disable parent term and automatic sorting of term
// include_once( 'functions/ajax/function-ajax-script.php' );
