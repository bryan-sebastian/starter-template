<?php
if ( ! function_exists( 'afterSetupTheme' ) ) {

	function afterSetupTheme(){

		// Enable support for Post Thumbnails, and declare two sizes.
		add_theme_support( 'post-thumbnails' );

		//Custom image sizes
		//add_image_size( 'homepage-thumb', 148, 180 );
		
		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		) );

		// This theme uses its own gallery styles.
		add_filter( 'use_default_gallery_style', '__return_false' );
	}

	add_action( 'after_setup_theme', 'afterSetupTheme' );
}