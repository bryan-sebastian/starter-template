<?php
/**
 * Header image
 **/
add_theme_support( 'custom-header', apply_filters( 'custom_header_args', array(
	'default-text-color'     => 'fff',
	'width'                  => 200,
	'flex-width'			 => true,
	'height'                 => 200,
	'flex-height'            => true,
) ) );

/**
 * Custom Background
 */
$defaults = array(
'default-color'          => '',
'default-image'          => '',
'wp-head-callback'       => '_custom_background_cb',
'admin-head-callback'    => '',
'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $defaults );